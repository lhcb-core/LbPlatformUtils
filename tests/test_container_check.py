#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2018 CERN                                                     #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function

import distutils.spawn
import os
import re
import subprocess
from contextlib import contextmanager

import pytest

try:
    from unittest import mock
except ImportError:
    import mock

try:
    from tempfile import TemporaryDirectory
except ImportError:
    from backports.tempfile import TemporaryDirectory


@contextmanager
def reroot_cvm(root):
    from LbPlatformUtils import inspect

    try:
        bkup = inspect.APPTAINER_ROOTS
        inspect.APPTAINER_ROOTS = [
            (path.replace(inspect._root, root), os_id)
            for path, os_id in inspect.APPTAINER_ROOTS
        ]
        for d, _ in inspect.APPTAINER_ROOTS:
            if not os.path.exists(d):
                os.makedirs(d)
        yield
    finally:
        inspect.APPTAINER_ROOTS = bkup


@mock.patch("LbPlatformUtils.inspect.check_output")
def test_apptainer_fake(check_output):
    from LbPlatformUtils import inspect

    with TemporaryDirectory() as temp_dir:
        with reroot_cvm(temp_dir):
            check_output.return_value = os.getcwd().encode()

            # test no apptainer
            check_output.side_effect = inspect.CalledProcessError(1, ["dummy"])
            assert inspect.apptainer_os_ids() == []

            check_output.side_effect = lambda *_args, **_kwargs: mock.DEFAULT
            assert inspect.apptainer_os_ids() == inspect.APPTAINER_ROOTS

            from LbPlatformUtils import get_viable_containers
            from LbPlatformUtils.describe import platform_info

            assert "apptainer" in get_viable_containers(
                platform_info(), "x86_64-slc6-gcc49-opt"
            )

            from LbPlatformUtils import dirac_platform
            from LbPlatformUtils.inspect import architecture

            arch = architecture()
            assert "-".join([arch, "any"]) != dirac_platform()
            assert "-".join([arch, "any"]) != dirac_platform(allow_containers=False)
            assert "-".join([arch, "any"]) == dirac_platform(allow_containers=True)

            check_output.return_value = b"/some/other/directory"
            assert inspect.apptainer_os_ids() == []


def test_apptainer_real():
    from LbPlatformUtils import inspect

    if not os.path.isdir("/cvmfs/cernvm-prod.cern.ch"):
        pytest.skip("missing cernvm")
    if not distutils.spawn.find_executable("apptainer"):
        pytest.skip("missing apptainer")

    assert inspect.apptainer_os_ids() == inspect.APPTAINER_ROOTS


@mock.patch("LbPlatformUtils.inspect.check_output")
def test_singularity_fake(check_output):
    from LbPlatformUtils import inspect

    with TemporaryDirectory() as temp_dir:
        with reroot_cvm(temp_dir):
            check_output.return_value = os.getcwd().encode()

            # test no singularity
            check_output.side_effect = inspect.CalledProcessError(1, ["dummy"])
            assert inspect.singularity_os_ids() == []

            check_output.side_effect = lambda *_args, **_kwargs: mock.DEFAULT
            assert inspect.singularity_os_ids() == inspect.APPTAINER_ROOTS

            from LbPlatformUtils import get_viable_containers
            from LbPlatformUtils.describe import platform_info

            assert "singularity" in get_viable_containers(
                platform_info(), "x86_64-slc6-gcc49-opt"
            )

            from LbPlatformUtils import dirac_platform
            from LbPlatformUtils.inspect import architecture

            arch = architecture()
            assert "-".join([arch, "any"]) != dirac_platform()
            assert "-".join([arch, "any"]) != dirac_platform(allow_containers=False)
            assert "-".join([arch, "any"]) == dirac_platform(allow_containers=True)

            check_output.return_value = b"/some/other/directory"
            assert inspect.singularity_os_ids() == []


def test_singularity_real():
    from LbPlatformUtils import inspect

    if not os.path.isdir("/cvmfs/cernvm-prod.cern.ch"):
        pytest.skip("missing cernvm")
    if not distutils.spawn.find_executable("singularity"):
        pytest.skip("missing singularity")

    assert inspect.singularity_os_ids() == inspect.APPTAINER_ROOTS
