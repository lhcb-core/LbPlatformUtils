#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

# Print each command before it is ran
set -x

if which python >& /dev/null ; then
    if python -c 'import sys; exit(0 if sys.version_info < (3, 7) else 1)' ; then
        # Poetry only supports Python >= 3.7, in the other cases we use a different approach
        exec $(dirname $0)/basic_test.sh
    fi

    PY_VERS=$(python -c 'import sys; print(sys.version.split()[0])')
else
    if python3 -c 'import sys; exit(0 if sys.version_info < (3, 7) else 1)' ; then
        # Poetry only supports Python >= 3.7, in the other cases we use a different approach
        exec $(dirname $0)/basic_test.sh
    fi

    PY_VERS=$(python3 -c 'import sys; print(sys.version.split()[0])')
fi

. .ci/ensure_poetry.sh

poetry install --sync

poetry run python3 --version

if which singularity >/dev/null 2>&1 ; then
    singularity --version
fi

case $PY_VERS in
    3.11.*)
        # 2022-10-27: pytest-cov crashes with Python 3.11
        poetry run pytest
        ;;
    *)
        poetry run pytest --cov=LbPlatformUtils --cov-report=term --cov-report=html
        ;;
esac

if [ -d cover ] ; then
    mkdir -p cover_report && mv -f cover "cover_report/${CI_JOB_NAME:-local}"
fi

if which apptainer >/dev/null 2>&1 ; then
    apptainer --version
fi

poetry run lb-describe-platform --flags

poetry run lb-debug-platform --no-exit-with-error-count --help
poetry run lb-debug-platform --no-exit-with-error-count --debug
poetry run lb-debug-platform --no-exit-with-error-count --output=platform-info.json
