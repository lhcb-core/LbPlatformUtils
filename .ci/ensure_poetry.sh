# Helper to make sure we have a working version of Poetry
# in the environment
export PIPX_BIN_DIR="$PWD/.local/bin"
export XDG_DATA_HOME="$PWD/.local/share"
export XDG_CONFIG_HOME="$PWD/.config"
export XDG_CACHE_HOME="$PWD/.cache"
export PATH="$PIPX_BIN_DIR:$PATH"

if ! poetry --version >/dev/null 2>&1  ; then
    python3 -m ensurepip
    python3 -m pip install pipx
    pipx install poetry
fi
